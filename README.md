# bash-newfile #

## Overview ##
This package, *bash-newfile*, is a simple script to handle the creation of new
files from predefined templates. It will start out as a simple way to do __cp
$template $newfile__, as well as provide some other basic funtionality, such as
listing the available filetypes.
